/*
   Copyright 2015 Alexander Haas
   This file is part of Sirma

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdexcept>
#include <QObject>
#include <QString>
#include "ConnectionTypeRdesktop.hpp"

QString ConnectionTypeRdesktop::getFullCommand(const ConnectionData& connectionData)
{   
    QString fullCommand = "";
    
    // set the command itself
    if (m_command.isEmpty())
    {
        fullCommand = "rdesktop";
    }
    else
    {
        fullCommand = m_command;
    }
    
    
    // set the keyboard, if given
    if (!connectionData.keyboard.isEmpty())
    {
        fullCommand += " -k ";
        fullCommand += connectionData.keyboard;
    }
    
    
    // set the colordepth, if given
    if (!connectionData.colordepth.isEmpty())
    {
        // remove leading and trailing whitespaces
        QString colordepthTrimmed=connectionData.colordepth.trimmed();
        
        if (!isColordepthValid(colordepthTrimmed))
        {
            throw std::invalid_argument(QObject::tr("invalid color depth").toStdString());
        }
        
        fullCommand += " -a ";
        fullCommand += colordepthTrimmed;
    }
    
    
     // set fullscreen, if given 
    if (connectionData.wantsFullscreen)
    {
        fullCommand += " -f";        
    }
    else // otherwise set horz and vert pixel, if given
    {   
        if (!connectionData.horizpixel.isEmpty() && !connectionData.vertpixel.isEmpty())
        {
            // remove leading and trailing whitespaces
            QString horizPixelTrimmed=connectionData.horizpixel.trimmed();
            QString vertPixelTrimmed=connectionData.vertpixel.trimmed();
            
            if (!isGeometryValid(horizPixelTrimmed,vertPixelTrimmed))
            {
                throw std::invalid_argument(QObject::tr("invalid window geometry").toStdString());
            }
        
            fullCommand += " -g ";
            fullCommand += horizPixelTrimmed;
            fullCommand += "x";
            fullCommand += vertPixelTrimmed;
        }
    }
        
        
    // set domain, if given
    if (!connectionData.domain.isEmpty())
    {     
        fullCommand += " -d ";
        fullCommand += connectionData.domain;
    }
    
    
    // set username, if given
    if (!connectionData.username.isEmpty())
    {     
        fullCommand += " -u ";
        fullCommand += connectionData.username;
    }
   
    
    // set password, if given
    if (!connectionData.password.isEmpty())
    {
        if (connectionData.password=="-")
        {
            throw std::invalid_argument(QObject::tr("interactive password input is not supported").toStdString());
        }
        
        fullCommand += " -p ";
        fullCommand += connectionData.password;
    }    
   
    
    // clipboard is ignored for rdesktop
   
    
    // set machine
    if (!connectionData.machine.isEmpty())
    {
        ///@todo additional validity tests may be useful
        fullCommand += " ";
        fullCommand += connectionData.machine;
    }
    else
    {
        throw std::invalid_argument(QObject::tr("target machine missing").toStdString());
    }
    
    return(fullCommand);    
};



bool ConnectionTypeRdesktop::isColordepthValid(const QString& colordepth)
{
    if (colordepth == "8" ||
        colordepth == "15" ||
        colordepth == "16" ||
        colordepth == "24" ||
        colordepth == "32" )
    {
        return(true);
    }
    return(false);
}



bool ConnectionTypeRdesktop::isGeometryValid(const QString& horizPixel, const QString& vertPixel)
{
    // horiz AND vert shall be defined
    if(horizPixel.isEmpty() && !vertPixel.isEmpty()) return(false);
    if(!horizPixel.isEmpty() && vertPixel.isEmpty()) return(false);
    
    bool numberOk=false;
    unsigned int pixelValue=0;
    
    // horiz and vert pixel limits are set to MS server 2008R2 limits
    // see https://technet.microsoft.com/en-us/library/cc772472%28v=ws.10%29.aspx
    pixelValue=horizPixel.toUInt(&numberOk);
    if(!numberOk) return(false);
    if(pixelValue>4096) return(false);
    
    pixelValue=vertPixel.toUInt(&numberOk);
    if(!numberOk) return(false);
    if(pixelValue>2048) return(false);
    
    // if we reach this point, everything was fine :-)
    return(true);
}