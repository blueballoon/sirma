#ifndef __I_CONNECTION_TYPE_HPP__
#define __I_CONNECTION_TYPE_HPP__
/*
   Copyright 2015 Alexander Haas
   This file is part of Sirma

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QString>
#include "ConfigDataTypes.hpp"

class IConnectionType
{
public:    
    virtual ~IConnectionType() {};
    
    ///@brief with the given parameters, builds the complete rdp client command line
    ///@param connectionData the params from which the commad shall be built
    ///@return the command line ready for execution
    ///@throw std::invalid_argument if any argument has an illegal value
    ///@throw std::invalid_argument if any mandatory argument is empty
    virtual QString getFullCommand(const ConnectionData& connectionData) = 0;
};

#endif