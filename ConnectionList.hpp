#ifndef __CONNECTION_LIST_HPP__
#define __CONNECTION_LIST_HPP__

/*
   Copyright 2015 Alexander Haas
   This file is part of Sirma

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QList>
#include <QHash>
#include <QString>

// forward declarations
class Connection;



class ConnectionList
{
    Q_OBJECT
    
public:  // types
    struct ConnectionInfo
    {
        QString connectionName;
        QString connectionStatus;
        int id;
    };
    
public:  // methods
    ConnectionList();
    ~ConnectionList();
    
    ///@brief to be used from connection widget
    QList<ConnectionList::ConnectionInfo> getConnections;  

public slots:
    void slotRunConnection(int id);
    
signals: 
    void signalConnectionStatusChanged(int id, const QString& status);
    
private:
    QHash<int, Connection*> m_connectionList;
};

#endif