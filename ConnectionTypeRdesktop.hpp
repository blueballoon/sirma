#ifndef __CONNECTION_TYPE_RDESKTOP_HPP__
#define __CONNECTION_TYPE_RDESKTOP_HPP__

/*
   Copyright 2015 Alexander Haas
   This file is part of Sirma

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QString>
#include "IConnectionType.hpp"


///@brief rdesktop implementation of IConnectionType
class ConnectionTypeRdesktop: public IConnectionType
{
public:  // methods
    ConnectionTypeRdesktop(const QString& command):
        m_command(command) {}
        
    virtual  ~ConnectionTypeRdesktop() {};
    
    virtual QString getFullCommand(const ConnectionData& connectionData);
    
private:  // methods
    ///@brief checks if the given colordepth string is valid
    ///@return true if the colordepth string is valid, false otherwise
    bool isColordepthValid(const QString& colordepth);
    
    ///@brief checks if the given geometry strings are valid
    ///@return true if the horiz and vert pixel strings is valid, false otherwise
    bool isGeometryValid(const QString& horizPixel, const QString& vertPixel);
    
private:  // attribs
    const QString m_command;
};

#endif