/*
   Copyright 2015 Alexander Haas
   This file is part of Sirma

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QtTest/QtTest>
#include <QString>
#include <iostream>
#include <set>
#include "ConfigDataTypes.hpp"
#include "ConnectionTypeRdesktop.hpp"
#include "ConfigReader.hpp"

class TestConnectionTypeRdesktop: public QObject
{
    Q_OBJECT
    private slots:
        void testCommandGeneration();


};

void TestConnectionTypeRdesktop::testCommandGeneration()
{
    ConnectionData connectionData1;
    connectionData1.machine = "192.168.1.123";
    connectionData1.keyboard = "de";
    connectionData1.horizpixel = "1024";
    connectionData1.vertpixel = "768";
    connectionData1.colordepth = "16";
    connectionData1.domain = "local";
    connectionData1.username = "halex";
    connectionData1.password = "VerY_H1gH_5ecur3";
    connectionData1.wantsFullscreen = false;
    connectionData1.wantsClipboard = false;
    
    ConnectionTypeRdesktop rdesktopConn1("/usr/bin/rdesktop");
    QString commandLine1 = rdesktopConn1.getFullCommand(connectionData1);
    
    qDebug() << commandLine1;
    //QCOMPARE(commandLine1, QString("jsdlökasjdsadk") );
}



class TestConfigReader: public QObject
{
    Q_OBJECT
    private slots:
        void testSimpleConfigParsing();


};



void TestConfigReader::testSimpleConfigParsing()
{
    ConfigReader cfgReader("sirmarc");
    QList<ConnectionData>  connData = cfgReader.getConnectionData();
    QList<ClientData> clientData = cfgReader.getClientData();
    
    for(int currClientIndex = 0; currClientIndex < clientData.size(); ++currClientIndex)
    {
        ClientData currClientData = clientData.at(currClientIndex);
        QCOMPARE(currClientData.command,  QString("/usr/bin/rdesktop") );
        QCOMPARE(currClientData.clientType, clientRdesktop );    
    }
    
    for(int currConnIndex = 0; currConnIndex < connData.size(); ++currConnIndex)
    {
        ConnectionData currConnData = connData.at(currConnIndex);
        QCOMPARE(currConnData.displayName, QString("scaleo") );
        QCOMPARE(currConnData.machine, QString("192.168.1.104") );
        QCOMPARE(currConnData.keyboard, QString("de") );
        QCOMPARE(currConnData.horizpixel, QString("1024") );
        QCOMPARE(currConnData.vertpixel, QString("768") );
        QCOMPARE(currConnData.colordepth, QString("16") );
        QCOMPARE(currConnData.domain, QString("") );
        QCOMPARE(currConnData.username, QString("halex") );
        QCOMPARE(currConnData.password, QString("") );
        QCOMPARE(currConnData.wantsFullscreen, false );
        QCOMPARE(currConnData.wantsClipboard, true );
        QCOMPARE(currConnData.clientType, clientRdesktop );  
    }
}

// dont use QTEST_MAIN as we want to use and execute several test classes here 
//QTEST_MAIN(TestSyscallProcessor)
 #include "unittest.moc"


int main()
{
    std::cout << "unit test for sirma \n" << std::endl;
    
    TestConnectionTypeRdesktop test1;
    QTest::qExec(&test1);
    
    TestConfigReader test2;
    QTest::qExec(&test2);
    

    return(0);
}
