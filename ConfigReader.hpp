#ifndef __CONFIG_READER_HPP__
#define __CONFIG_READER_HPP__

/*
   Copyright 2015 Alexander Haas
   This file is part of Sirma

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QString>
#include <QList>
#include "ConfigDataTypes.hpp"

// forward decls
class QDomElement;
class SirmaConfigParser;
class ConnectionList;


///@brief reads the config file and provides access to the config data
class ConfigReader
{
public:
    ///@brief constructor, binds the instance to the given config file
    ///@param configFileName the config file that shall be used; if the given string
    ///       is empty, ~/.sirmarc is used
    ConfigReader(QString configFileName);
    
    ///@brief destructor, releases the config file
    ~ConfigReader();

    //maybe obsolete
    ///@brief reads all connection data from the config file and returnes the parsed result
    ///@return all stored connection definitions as list of structs; one connecion per struct
    ///@throw invalid_argument when detecting config errors in the sirma xml confg file. Use what to get detailed information
    ///@throw runtime_error on problems accessing the file or parsing the xml structure itself
    QList<ConnectionData> getConnectionData();
    
    //maybe obsolete
    ///@brief reads all client data from the config file and returnes the parsed result
    ///@return all stored client definitions  as list of structs; one client per struct
    ///@throw invalid_argument when detecting config errors in the sirma xml confg file. Use what to get detailed information
    ///@throw runtime_error on problems accessing the file or parsing the xml structure itself    
    QList<ClientData> getClientData();

    
    //use ConfigReader as factory for ConnectionList
    ConnectionList* getConnectionList();
    
private: //attribs

    ///@brief pimpl
    SirmaConfigParser* m_sirmaConfigParser; 
};

#endif
