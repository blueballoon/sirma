#ifndef __CONNECTION_DATA_HPP__
#define __CONNECTION_DATA_HPP__

/*
   Copyright 2015 Alexander Haas
   This file is part of Sirma

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <QString>

enum ClientType
{
    clientRdesktop,
    clientXfreeRdp
};

///@brief plain struct to carry the connection data from xml reader to connection type
struct ConnectionData
{
    QString displayName;   // GUI display name of the connection
    ClientType clientType; // client to use for this connection
    QString machine;       // target machine
    QString keyboard;      // keyboard map
    QString horizpixel;    // horizontal window size in pixel
    QString vertpixel;     // vertical window size in pixel
    QString colordepth;    // window color depth
    QString domain;        // windows logon domain
    QString username;      // windows user name
    QString password;      // windows password
    bool wantsFullscreen;  // fullscreen connection
    bool wantsClipboard;   // enable clipboard
};


struct ClientData
{
    ClientType clientType;
    QString command;
};

#endif
