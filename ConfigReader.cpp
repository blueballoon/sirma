/*
   Copyright 2015 Alexander Haas
   This file is part of Sirma

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <QFile>
#include <QDomDocument>
#include <QObject>
#include <stdexcept>
#include <set>
#include "ConnectionList.hpp"
#include "ConfigReader.hpp"

/////////////////////////////////////////////////////////////////////////////////////////
// decl and impl of sirmaConfigParser
/////////////////////////////////////////////////////////////////////////////////////////


///@brief sirmaConfigParser pimpl class
///@todo maybe use map instead of struct for connection and client data,
///fill the map directly with xml content (tagname=key, tagvalue=value)
///and let the connection perform all validity checks
///Advantage: easy connectiontype individual configuration, more flexible, less boilerplate code
///Disadvantage: possible less precise xml config failure diagnostics
class SirmaConfigParser
{
public:
    ///@brief constructor, only sets the filename, uses default ~/.sirmarc if string is empty
    SirmaConfigParser(QString configFileName);

    ///@brief reads the config file, parses it and extract all connection and client date
    ///@throw invalid_argument when detecting config errors in the sirma xml confg file. Use what to get detailed information
    ///@throw runtime_error on problems accessing the file itself
    ///@throw runtime_error on problems parsing the file (eg xml struture errors)
    void readAndParseConfigfile();
    
    ///@brief parse the given ClientConfig element completely
    ///@throw see readAndParseConfigfile
    void parseSirmaClientConfigElement(const QDomElement& element);
    
    ///@brief parse the given ConnectionConfig element completely
    ///@throw see readAndParseConfigfile    
    void parseSirmaConnectionConfigElement(const QDomElement& element);

    void parseSirmaClientElement(const QDomElement& element);
    void parseSirmaConnectionElement(const QDomElement& element);

    QList<ConnectionData> getConnectionData();
    QList<ClientData> getClientData();

private: // methods
    ///@throw invalid_argument when give string does not match any valid enum
    ClientType getIdEnum(QString idString);

    ///@throw invalid_argument with given error text when flag was already set
    void checkAndSetFoundFlag(std::set<QString>& flags,
                                             const QString& currentFlag,
                                             const QString& errorText);
    
    ///@throw invalid_argument if given string is neither "true" or "false"
    bool parseBooleanString(QString boolString);

private:  // pimpl data
    QString m_configFileName;
    bool m_parsingDone;  // used for lazy eval
    QList<ConnectionData> m_connectionData;
    QList<ClientData> m_clientData;
};




SirmaConfigParser::SirmaConfigParser(QString configFileName)
{
    m_configFileName=configFileName;
    if (configFileName.isEmpty())
    {
         m_configFileName="~/.sirmarc";
    }

    m_parsingDone = false;
}

void SirmaConfigParser::readAndParseConfigfile()
{
    // open file
    QFile configQFile(m_configFileName);
    if(!configQFile.open(QFile::ReadOnly | QFile::Text))
    {
        throw std::runtime_error(QObject::tr("cannot open config file").toStdString());
    }

    // parse and build dom
    QString domErrorString;
    int errorLine;
    int errorColumn;
    QDomDocument configDomDoc;
    if (!configDomDoc.setContent(&configQFile,
                                 false, // no namespace processing
                                &domErrorString,
                                &errorLine, &errorColumn))
    {
        configQFile.close();
        throw std::runtime_error(QObject::tr("error on parsing config file").toStdString());
    }
    configQFile.close();

    // traverse thru dom and extract the configuration
    QDomElement rootElement = configDomDoc.documentElement();
    if (rootElement.tagName() != "sirma")
    {
        throw std::invalid_argument(QObject::tr("XML root element <sirma> not found in config file").toStdString());
    }


    QDomNode child = rootElement.firstChild();
    while (!child.isNull())
    {
        if (child.toElement().tagName() == "clientconfig")
        {
            parseSirmaClientConfigElement(child.toElement());
        }
        else if (child.toElement().tagName() == "connectionconfig")
        {
            parseSirmaConnectionConfigElement(child.toElement());
        }
        child = child.nextSibling();
    }
    
    m_parsingDone = true;

}


void SirmaConfigParser::parseSirmaClientConfigElement(const QDomElement& element)
{
    QDomNode child = element.firstChild();
    while (!child.isNull())
    {
        if (child.toElement().tagName() == "client")
        {
            parseSirmaClientElement(child.toElement());
        }
        child = child.nextSibling();
    }
}



void SirmaConfigParser::parseSirmaClientElement(const QDomElement& element)
{
    ClientData currentClient;

    // flags to check if mandatory tags or multiple definitions were found
    std::set<QString> foundFlags;

    QDomNode child = element.firstChild();
    while (!child.isNull())
    {
        if (child.toElement().tagName() == "id")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple id tags found in client tag"));
            QString clientIdString = child.toElement().text();
            currentClient.clientType = getIdEnum(clientIdString);
        }
        else if (child.toElement().tagName() == "command")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple command tags found in client tag"));
            currentClient.command = child.toElement().text();
        }

        child = child.nextSibling();
    }

    // check for mandatory flags
    if (foundFlags.count("id") != 0 &&
        foundFlags.count("command") != 0 )
    {
        m_clientData.push_back(currentClient);
    }
    else
    {
        throw std::invalid_argument(QObject::tr("Incomplete client tag definition").toStdString());
    }
}



void SirmaConfigParser::parseSirmaConnectionConfigElement(const QDomElement& element)
{
    QDomNode child = element.firstChild();
    while (!child.isNull())
    {
        if (child.toElement().tagName() == "connection")
        {
            parseSirmaConnectionElement(child.toElement());
        }
        child = child.nextSibling();
    }
}



void SirmaConfigParser::parseSirmaConnectionElement(const QDomElement& element)
{
    ConnectionData currentConnection;

    // flags to check if mandatory tags or multiple definitions were found
    std::set<QString> foundFlags;


    QDomNode child = element.firstChild();
    while (!child.isNull())
    {
        if (child.toElement().tagName() == "name")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple name tags found in connection tag"));
            currentConnection.displayName = child.toElement().text();
        }
        else if (child.toElement().tagName() == "clientid")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple clientid tags found in connection tag"));
            QString clientIdString = child.toElement().text();
            currentConnection.clientType = getIdEnum(clientIdString);
        }
        else if (child.toElement().tagName() == "targetmachine")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple targetmachine tags found in connection tag"));
            currentConnection.machine = child.toElement().text();
        }
        else if (child.toElement().tagName() == "keyboard")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple keyboard tags found in connection tag"));
            currentConnection.keyboard = child.toElement().text();
        }
        else if (child.toElement().tagName() == "horizpixel")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple horizpixel tags found in connection tag"));
            currentConnection.horizpixel = child.toElement().text();
        }
        else if (child.toElement().tagName() == "vertpixel")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple vertpixel tags found in connection tag"));
            currentConnection.vertpixel = child.toElement().text();
        }
        else if (child.toElement().tagName() == "colordepth")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple colordepth tags found in connection tag"));
            currentConnection.colordepth = child.toElement().text();
        }
        else if (child.toElement().tagName() == "domain")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple domain tags found in connection tag"));
            currentConnection.domain = child.toElement().text();
        }
        else if (child.toElement().tagName() == "username")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple username tags found in connection tag"));
            currentConnection.username = child.toElement().text();
        }
        else if (child.toElement().tagName() == "password")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple password tags found in connection tag"));
            currentConnection.password = child.toElement().text();
        }
        else if (child.toElement().tagName() == "fullscreen")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple fullscreen tags found in connection tag"));
            currentConnection.wantsFullscreen = parseBooleanString(child.toElement().text());
        }
        else if (child.toElement().tagName() == "clipboard")
        {
            checkAndSetFoundFlag(foundFlags,child.toElement().tagName(),
                                 QObject::tr("Multiple clipboard tags found in connection tag"));
            currentConnection.wantsClipboard = parseBooleanString(child.toElement().text());
        }
        child = child.nextSibling();
    }

    // check for mandatory flags
    if (foundFlags.count("name") != 0 &&
        foundFlags.count("clientid") != 0 &&
        foundFlags.count("targetmachine") != 0)
    {
        m_connectionData.push_back(currentConnection);
    }
    else
    {
        throw std::invalid_argument(QObject::tr("Incomplete connection tag definition").toStdString());
    }
}



QList<ConnectionData> SirmaConfigParser::getConnectionData()
{
    if(!m_parsingDone)
    {
        readAndParseConfigfile();
    }
    return(m_connectionData);
}



QList<ClientData> SirmaConfigParser::getClientData()
{
    if(!m_parsingDone)
    {
        readAndParseConfigfile();
    }
    return(m_clientData);    
}



ClientType SirmaConfigParser::getIdEnum(QString idString)
{
    ClientType currentClientType;
    if (idString == "rdesktop")
    {
        currentClientType = clientRdesktop;
    }
    else if (idString == "xfreerdp")
    {
        currentClientType = clientXfreeRdp;
    }
    else
    {
        throw std::invalid_argument(QObject::tr("Unsupported id tag found").toStdString());
    }
    return(currentClientType);
}



void SirmaConfigParser::checkAndSetFoundFlag(std::set<QString>& flags,
                                             const QString& currentFlag,
                                             const QString& errorText)
{
    if (flags.count(currentFlag) == 0)
    {
        flags.insert(currentFlag);
    }
    else
    {
        throw std::invalid_argument(errorText.toStdString());
    }
}



bool SirmaConfigParser::parseBooleanString(QString boolString)
{
    if (boolString == "true")
    {
        return(true);
    }
    else if (boolString == "false")
    {
        return(false);
    }
    else
    {
        throw std::invalid_argument(QObject::tr("invalid boolean value, must be true or false").toStdString());
    }
}





/////////////////////////////////////////////////////////////////////////////////////////
// impl of ConfigReader
/////////////////////////////////////////////////////////////////////////////////////////



ConfigReader::ConfigReader(QString configFileName)
{
    m_sirmaConfigParser = new SirmaConfigParser(configFileName);
}



ConfigReader::~ConfigReader()
{
    delete m_sirmaConfigParser;
}



QList<ConnectionData> ConfigReader::getConnectionData()
{
    return(m_sirmaConfigParser->getConnectionData());
}



QList<ClientData> ConfigReader::getClientData()
{
    return(m_sirmaConfigParser->getClientData());
}



ConnectionList* ConfigReader::getConnectionList()
{
    QList<ConnectionData> connectionData = m_sirmaConfigParser->getConnectionData();
    QList<ClientData> clientData = m_sirmaConfigParser->getClientData();
    
    // who will become owner of client definitions??
}



