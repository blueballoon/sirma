#ifndef __CONNECTION_HPP__
#define __CONNECTION_HPP__

/*
   Copyright 2015 Alexander Haas
   This file is part of Sirma

    Sonoran is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Sonoran is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Sonoran.  If not, see <http://www.gnu.org/licenses/>.
*/

// forward declarations
class IConnectionType;

class Connection
{
public:
    Connection(int id, const IConnectionType* connectionType):
        m_id(id),
        m_connectionType(connectionType);
        
    virtual  ~Connection();
    
    void startConnection();
    QString getConnectionName();
    QString getConnectionStatus();
    
    
private:  // types
    enum StatusMode = { running, notrunning }; 
    
private:  //attribs
    const int m_id;
    const IConnectionType* m_connectionType;
    
    StatusMode m_statusMode;
    int m_pid;
}


#endif